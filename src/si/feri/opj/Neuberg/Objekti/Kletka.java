package si.feri.opj.Neuberg.Objekti;

import java.io.Serializable;

import si.feri.opj.Neuberg.Kmetija.Zival;
import si.feri.opj.Neuberg.Ostalo.PremladaZivalException;
import si.feri.opj.Neuberg.Ostalo.TipZivaliException;

public class Kletka extends Ograda implements Serializable{

	private boolean jeVecji;

	public Kletka(String naziv, boolean jeVecji, int kapaciteta, si.feri.opj.Neuberg.Kmetija.TipZivali TipZivali) throws TipZivaliException {
		super(naziv, kapaciteta, TipZivali);
		this.jeVecji = jeVecji;

		if(this.TipZivali==si.feri.opj.Neuberg.Kmetija.TipZivali.RIBA) {
			throw new TipZivaliException();
		}
		
		if (this.jeVecji == true) {
			super.kapaciteta = kapaciteta * 2;
			zivali=new Zival[kapaciteta*2];
		} else
			super.kapaciteta = kapaciteta;
		    zivali=new Zival[kapaciteta];
	}

	public double vrniKapaciteto() {

		return this.kapaciteta;
	}

	@Override
	public String toString() {
		return "Kletka [jeVecji=" + jeVecji + ", " + super.toString() + "]";
	}

	public boolean isJeVecji() {
		return jeVecji;
	}

	public void setJeVecji(boolean jeVecji) {
		this.jeVecji = jeVecji;
	}

}
