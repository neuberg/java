package si.feri.opj.Neuberg.Objekti;

import java.io.Serializable;
import java.util.Arrays;

import si.feri.opj.Neuberg.Kmetija.TipZivali;
import si.feri.opj.Neuberg.Kmetija.Zival;
import si.feri.opj.Neuberg.Ostalo.IVstop;
import si.feri.opj.Neuberg.Ostalo.OgradaPolnaException;
import si.feri.opj.Neuberg.Ostalo.PremladaZivalException;
import si.feri.opj.Neuberg.Ostalo.TipZivaliException;

public abstract class Ograda implements Serializable{

	private String naziv;
	protected int kapaciteta;
	protected Zival[] zivali;
	TipZivali TipZivali;

	@Override
	public String toString() {
		return "Ograda [naziv=" + naziv + ", kapaciteta=" + kapaciteta + ", zivali=" + Arrays.toString(zivali)
				+ ", TipZivali=" + TipZivali + "]";
	}

	private Ograda() {
	}

	private Ograda(String naziv) {
		this.naziv = naziv;
	}

	public Ograda(String naziv, int kapaciteta, TipZivali TipZivali)  {
		this(naziv);
		this.kapaciteta = kapaciteta;
		if(this.kapaciteta<0) {
			throw new IllegalArgumentException("Negativna vrednost ograde!");
		}
		zivali = new Zival[kapaciteta];
		this.TipZivali = TipZivali;
		
		
		
		}
	

	public void dodajZival(Zival zival) throws PremladaZivalException, OgradaPolnaException, TipZivaliException {

		if(zival.izracunajStarostZivali()<1) {
			throw new PremladaZivalException();
		}
		if(zival.getTipZivali()!=TipZivali) {
			throw new TipZivaliException();
		}
		else {
			for (int i = 0; i < zivali.length; i++) {
				if (zivali[i] == null) {
					zivali[i] = zival;
					
					break;
				}
				 if (zivali[zivali.length-1]!=null) {
					throw new OgradaPolnaException();
					
				}
			}
		}
		
	}

	public void odstraniZivali() {
		for (int i = 0; i < zivali.length; i++) {
			if (zivali[i] != null) {
				zivali[i] = null;
			}
		}
	}

	public double vrniZasedenost() {
		double stevec = 0;
		for (int i = 0; i < zivali.length; i++) {
			if (zivali[i] != null) {
				stevec = stevec + 1;
			}

		}
		double procent = (stevec / this.kapaciteta) * 100;
        if(procent<0) {
        	throw new IllegalArgumentException("Negativna vrednost zasedenosti!");
        }
		return procent;
	}

	public abstract double vrniKapaciteto();

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getKapaciteta() {
		return kapaciteta;
	}

	public void setKapaciteta(int kapaciteta) {
		this.kapaciteta = kapaciteta;
	}

	public Zival[] getZivali() {
		return zivali;
	}

	public void setZivali(Zival[] zivali) {
		this.zivali = zivali;
	}

	public TipZivali getTipZivali() {
		return TipZivali;
	}

	public void setTipZivali(TipZivali tipZivali) {
		TipZivali = tipZivali;
	}

}
