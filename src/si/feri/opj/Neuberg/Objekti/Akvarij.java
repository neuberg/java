package si.feri.opj.Neuberg.Objekti;

import java.io.Serializable;

import si.feri.opj.Neuberg.Ostalo.TipZivaliException;

public class Akvarij extends Ograda implements Serializable{

	
		
	public Akvarij(String naziv, int kapaciteta, si.feri.opj.Neuberg.Kmetija.TipZivali TipZivali) throws TipZivaliException {
		super(naziv, kapaciteta, TipZivali);
		
	if(this.TipZivali==si.feri.opj.Neuberg.Kmetija.TipZivali.SESALEC||this.TipZivali==si.feri.opj.Neuberg.Kmetija.TipZivali.PTIC) {
				throw new TipZivaliException();
			}
	
	}

	

	@Override
	public String toString() {
		return "Akvarij [ " + super.toString() + "]";
	}

	@Override
	public double vrniKapaciteto() {

		return this.getKapaciteta();
	}

}
