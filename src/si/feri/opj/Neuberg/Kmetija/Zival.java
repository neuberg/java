package si.feri.opj.Neuberg.Kmetija;

import java.io.Serializable; 
import java.time.LocalDate;
import java.time.Period;

import si.feri.opj.Neuberg.Objekti.Ograda;
import si.feri.opj.Neuberg.Ostalo.IVstop;
import si.feri.opj.Neuberg.Ostalo.PremladaZivalException;

public class Zival implements IVstop, Serializable {

	private String ime;
	private LocalDate datumRojstva;
	private double teza;
	private double dolzina;
	private String spol;
	private TipZivali TipZivali;// =TipZivali.RIBA;

	private Zival() {

	}

	private Zival(String ime, LocalDate datumRojstva, String spol) {

		this.ime = ime;
		this.datumRojstva = datumRojstva;
		this.spol = spol;
	}

	public Zival(String ime, LocalDate datumRojstva, double teza, double dolzina, String spol, TipZivali TipZivali) {

		this(ime, datumRojstva, spol);
		this.teza = teza;
		this.dolzina = dolzina;
		this.TipZivali = TipZivali;
		
		if(this.teza<0||this.dolzina<0) {
			throw new IllegalArgumentException("Negativna vrednost zivali!");
		}
		
	}
/*	boolean obstaja = false;
	for (int i = 0; i < zivali.length; i++) {
		if (zivali[i] != null) {
			if (zivali[i].getIme().equals(ime)) {
				obstaja = true;
			*/	
//
	@Override
	public boolean lahkoVztopi(Ograda ograda)  {
	  boolean vrni = false;
	for(int i=0;i<ograda.getKapaciteta();i++) {
		if(this.equals(ograda.getZivali()[i])) {
			vrni=true;
		}
		
	}
	return vrni;
		
	}

	public int izracunajStarostZivali() {
		LocalDate danes = LocalDate.now();
		Period starost = Period.between(datumRojstva, danes);

		return starost.getYears();

	}

	@Override
	public String toString() {
		return "Zival [ime=" + ime + ", datumRojstva=" + datumRojstva + ", teza=" + teza + ", dolzina=" + dolzina
				+ ", spol=" + spol + ", TipZivali=" + TipZivali + "]";
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public LocalDate getDatumRojstva() {
		return datumRojstva;
	}

	public void setDatumRojstva(LocalDate datumRojstva) {
		this.datumRojstva = datumRojstva;
	}

	public double getTeza() {
		return teza;
	}

	public void setTeza(double teza) {
		this.teza = teza;
	}

	public double getDolzina() {
		return dolzina;
	}

	public void setDolzina(double dolzina) {
		this.dolzina = dolzina;
	}

	public String getSpol() {
		return spol;
	}

	public void setSpol(String spol) {
		this.spol = spol;
	}

	public TipZivali getTipZivali() {
		return TipZivali;
	}

	public void setTipZivali(TipZivali tipZivali) {
		TipZivali = tipZivali;
	}

}
