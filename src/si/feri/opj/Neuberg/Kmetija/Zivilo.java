package si.feri.opj.Neuberg.Kmetija;

import java.io.Serializable;
import java.time.LocalDate;

public class Zivilo implements Serializable{

	private String naziv;
	private double teza;
	private LocalDate rokUporabe;

	private Zivilo() {

	}

	private Zivilo(String naziv, LocalDate rokUporabe) {

		this.naziv = naziv;
		this.rokUporabe = rokUporabe;
	}

	public Zivilo(String naziv, double teza, LocalDate rokUporabe) {

		this(naziv, rokUporabe);
		this.teza = teza;
		
		if(this.teza<0) {
			throw new IllegalArgumentException("Negativna vrednost zivila!");
		}
	}

	@Override
	public String toString() {
		return "Zivilo [naziv=" + naziv + ", teza=" + teza + ", rokUporabe=" + rokUporabe + "]";
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public double getTeza() {
		return teza;
	}

	public void setTeza(double teza) {
		this.teza = teza;
	}

	public LocalDate getRokUporabe() {
		return rokUporabe;
	}

	public void setRokUporabe(LocalDate rokUporabe) {
		this.rokUporabe = rokUporabe;
	}

}
