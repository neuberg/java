package si.feri.opj.Neuberg.ui;

import java.awt.EventQueue;  
import javax.swing.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.CardLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.JTree;
import javax.swing.JTextField;
import java.awt.FlowLayout;
import net.miginfocom.swing.MigLayout;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JRadioButton;
import javax.swing.JList;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.JSeparator;
import javax.swing.SpringLayout;
import java.awt.Insets;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
public class uporabniskiVmesnik {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;
	private JTextField textField_14;
	private JTextField textField_15;
	private JPanel panel_13;
	private JPanel panel_12;
	private JTextField textField_16;
	private JTextField textField_17;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					uporabniskiVmesnik window = new uporabniskiVmesnik();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public uporabniskiVmesnik() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 11, 434, 250);
		frame.getContentPane().add(tabbedPane);
		
		JPanel Ograda = new JPanel();
		tabbedPane.addTab("Ograda", null, Ograda, null);
		Ograda.setLayout(null);
		
		JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane_1.setBounds(0, 0, 429, 222);
		Ograda.add(tabbedPane_1);
		
		JPanel panel_3 = new JPanel();
		tabbedPane_1.addTab("Brisi/Spremeni", null, panel_3, null);
		panel_3.setLayout(null);
		
		JLabel lblIzbrisi = new JLabel("IZBRISI OGRADO:");
		lblIzbrisi.setBounds(10, 11, 89, 14);
		panel_3.add(lblIzbrisi);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(126, 8, 28, 20);
		panel_3.add(comboBox_1);
		
		JButton btnIzbrii = new JButton("IZBRI\u0160I");
		btnIzbrii.setBounds(325, 19, 89, 23);
		panel_3.add(btnIzbrii);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 53, 424, 2);
		panel_3.add(separator_1);
		
		JLabel lblSpremeniOgrado = new JLabel(" SPREMENI OGRADO:");
		lblSpremeniOgrado.setBounds(10, 66, 116, 14);
		panel_3.add(lblSpremeniOgrado);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setBounds(126, 63, 28, 20);
		panel_3.add(comboBox_2);
		
		JLabel lblNaziv_1 = new JLabel("NAZIV");
		lblNaziv_1.setBounds(210, 113, 46, 14);
		panel_3.add(lblNaziv_1);
		
		JLabel lblNewLabel = new JLabel("KAPACITETA");
		lblNewLabel.setBounds(10, 113, 72, 14);
		panel_3.add(lblNewLabel);
		
		textField_1 = new JTextField();
		textField_1.setBounds(192, 135, 86, 20);
		panel_3.add(textField_1);
		textField_1.setColumns(10);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setBounds(20, 135, 29, 20);
		panel_3.add(spinner_1);
		
		JLabel lblTipZivali = new JLabel("TIP ZIVALI");
		lblTipZivali.setBounds(104, 113, 65, 14);
		panel_3.add(lblTipZivali);
		
		JComboBox comboBox_3 = new JComboBox();
		comboBox_3.setBounds(114, 135, 28, 20);
		panel_3.add(comboBox_3);
		
		JButton btnSpremeni = new JButton("SPREMENI ");
		btnSpremeni.setBounds(325, 160, 89, 23);
		panel_3.add(btnSpremeni);
		
		
		
		JPanel panel = new JPanel();
		tabbedPane_1.addTab("Ustvari", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblTipOgrade = new JLabel("TIP OGRADE:");
		lblTipOgrade.setForeground(SystemColor.textHighlight);
		lblTipOgrade.setBounds(10, 11, 79, 14);
		panel.add(lblTipOgrade);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Akvarij");
		rdbtnNewRadioButton.setBounds(81, 7, 65, 23);
		panel.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnKletka = new JRadioButton("Kletka");
		rdbtnKletka.setBounds(142, 7, 65, 23);
		panel.add(rdbtnKletka);
		
		JLabel lblNaziv = new JLabel("NAZIV:");
		lblNaziv.setBounds(30, 51, 46, 14);
		panel.add(lblNaziv);
		
		JLabel lblKapaciteta = new JLabel("KAPACITETA:");
		lblKapaciteta.setBounds(142, 51, 79, 14);
		panel.add(lblKapaciteta);
		
		textField = new JTextField();
		textField.setBounds(10, 76, 86, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel label = new JLabel("TIP ZIVALI:");
		label.setForeground(SystemColor.textHighlight);
		label.setBounds(256, 11, 65, 14);
		panel.add(label);
		
	
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(331, 8, 28, 20);
		panel.add(comboBox);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(152, 76, 29, 20);
		panel.add(spinner);
		
		JButton btnUstvari = new JButton("USTVARI");
		btnUstvari.setBounds(325, 148, 89, 23);
		panel.add(btnUstvari);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 36, 424, 15);
		panel.add(separator);
		
		JLabel lblSamoKletka = new JLabel("samo kletka");
		lblSamoKletka.setForeground(Color.RED);
		lblSamoKletka.setBounds(243, 51, 65, 14);
		panel.add(lblSamoKletka);
		
		JLabel lblKapaciteta_1 = new JLabel("Kapaciteta:");
		lblKapaciteta_1.setBounds(243, 62, 55, 14);
		panel.add(lblKapaciteta_1);
		
		JRadioButton rdbtnTrue = new JRadioButton("True");
		rdbtnTrue.setBounds(304, 47, 55, 23);
		panel.add(rdbtnTrue);
		
		JRadioButton rdbtnFalse = new JRadioButton("False");
		rdbtnFalse.setBounds(359, 39, 55, 39);
		panel.add(rdbtnFalse);
		
		JPanel panel_7 = new JPanel();
		tabbedPane_1.addTab("Dodaj zivali", null, panel_7, null);
		
		JLabel lblDodajival = new JLabel("DODAJ �IVAL:");
		
		JComboBox comboBox_10 = new JComboBox();
		panel_7.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panel_7.add(lblDodajival);
		panel_7.add(comboBox_10);
		
		JLabel lblNewLabel_8 = new JLabel("V OGRADO:");
		panel_7.add(lblNewLabel_8);
		
		JComboBox comboBox_12 = new JComboBox();
		panel_7.add(comboBox_12);
		
		JButton btnDodaj = new JButton("DODAJ");
		panel_7.add(btnDodaj);
		
		JPanel panel_8 = new JPanel();
		tabbedPane_1.addTab("Odstrani zivali", null, panel_8, null);
		panel_8.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblOdstrani = new JLabel("ODSTRANI ZIVAL:");
		panel_8.add(lblOdstrani);
		
		JComboBox comboBox_13 = new JComboBox();
		panel_8.add(comboBox_13);
		
		JLabel lblIzOgrade = new JLabel("IZ OGRADE: ");
		panel_8.add(lblIzOgrade);
		
		JComboBox comboBox_14 = new JComboBox();
		panel_8.add(comboBox_14);
		
		JButton btnOdstrani = new JButton("ODSTRANI");
		panel_8.add(btnOdstrani);
		
		JPanel Oskrbnik = new JPanel();
		tabbedPane.addTab("Zaposleni", null, Oskrbnik, null);
		Oskrbnik.setLayout(null);
		
		JTabbedPane tabbedPane_2 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane_2.setBounds(0, 0, 429, 222);
		Oskrbnik.add(tabbedPane_2);
		
		JPanel panel_1 = new JPanel();
		tabbedPane_2.addTab("Ustvari", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("TIP ZAPOSLENEGA:");
		lblNewLabel_1.setForeground(SystemColor.textHighlight);
		lblNewLabel_1.setBounds(0, 11, 111, 14);
		panel_1.add(lblNewLabel_1);
		
		JComboBox comboBox_4 = new JComboBox();
		comboBox_4.setBounds(115, 8, 28, 20);
		panel_1.add(comboBox_4);
		
		JLabel lblIme = new JLabel("IME");
		lblIme.setBounds(35, 49, 46, 14);
		panel_1.add(lblIme);
		
		JLabel lblPriimek = new JLabel("PRIIMEK");
		lblPriimek.setBounds(159, 49, 46, 14);
		panel_1.add(lblPriimek);
		
		JLabel lblTelefonska = new JLabel("TELEFONSKA:");
		lblTelefonska.setBounds(280, 49, 86, 14);
		panel_1.add(lblTelefonska);
		
		textField_2 = new JTextField();
		textField_2.setBounds(10, 84, 86, 20);
		panel_1.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(135, 84, 86, 20);
		panel_1.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(280, 84, 86, 20);
		panel_1.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblSamoTipaVeterirar = new JLabel("samo tipa veterinar");
		lblSamoTipaVeterirar.setForeground(Color.RED);
		lblSamoTipaVeterirar.setBounds(153, 4, 130, 14);
		panel_1.add(lblSamoTipaVeterirar);
		
		JLabel lblStevilkaDovoljenja = new JLabel("Stevilka dovoljenja:");
		lblStevilkaDovoljenja.setBounds(153, 11, 117, 22);
		panel_1.add(lblStevilkaDovoljenja);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(0, 115, 424, 3);
		panel_1.add(separator_2);
		
		textField_5 = new JTextField();
		textField_5.setBounds(280, 8, 86, 20);
		panel_1.add(textField_5);
		textField_5.setColumns(10);
		
		JButton btnUstvari_1 = new JButton("USTVARI");
		btnUstvari_1.setBounds(309, 145, 89, 23);
		panel_1.add(btnUstvari_1);
		
		JPanel panel_2 = new JPanel();
		tabbedPane_2.addTab("Brisi/spremeni", null, panel_2, null);
		panel_2.setLayout(null);
		
		JLabel lblNewLabel_2 = new JLabel("IZBRI�I ZAPOSLENEGA");
		lblNewLabel_2.setBounds(10, 11, 137, 14);
		panel_2.add(lblNewLabel_2);
		
		JComboBox comboBox_5 = new JComboBox();
		comboBox_5.setBounds(157, 8, 28, 20);
		panel_2.add(comboBox_5);
		
		JButton btnIzbrii_1 = new JButton("IZBRI\u0160I");
		btnIzbrii_1.setBounds(325, 7, 89, 23);
		panel_2.add(btnIzbrii_1);
		
		JLabel lblSpremeniZaposlenega = new JLabel("SPREMENI ZAPOSLENEGA:");
		lblSpremeniZaposlenega.setBounds(10, 62, 137, 14);
		panel_2.add(lblSpremeniZaposlenega);
		
		JLabel lblIme_1 = new JLabel("IME:");
		lblIme_1.setBounds(40, 87, 46, 14);
		panel_2.add(lblIme_1);
		
		JLabel lblPriimek_1 = new JLabel("PRIIMEK");
		lblPriimek_1.setBounds(139, 87, 46, 14);
		panel_2.add(lblPriimek_1);
		
		JLabel lblTelefonska_1 = new JLabel("TELEFONSKA");
		lblTelefonska_1.setBounds(233, 87, 64, 14);
		panel_2.add(lblTelefonska_1);
		
		textField_6 = new JTextField();
		textField_6.setBounds(10, 112, 86, 20);
		panel_2.add(textField_6);
		textField_6.setColumns(10);
		
		textField_7 = new JTextField();
		textField_7.setBounds(117, 112, 86, 20);
		panel_2.add(textField_7);
		textField_7.setColumns(10);
		
		textField_8 = new JTextField();
		textField_8.setBounds(213, 112, 86, 20);
		panel_2.add(textField_8);
		textField_8.setColumns(10);
		
		JLabel lblTip = new JLabel("TIP:");
		lblTip.setBounds(345, 87, 46, 14);
		panel_2.add(lblTip);
		
		JComboBox comboBox_6 = new JComboBox();
		comboBox_6.setBounds(355, 112, 28, 20);
		panel_2.add(comboBox_6);
		
		JLabel lblVeterinar = new JLabel("veterinar stevilka:");
		lblVeterinar.setForeground(Color.RED);
		lblVeterinar.setBounds(10, 156, 97, 14);
		panel_2.add(lblVeterinar);
		
		textField_9 = new JTextField();
		textField_9.setBounds(117, 153, 86, 20);
		panel_2.add(textField_9);
		textField_9.setColumns(10);
		
		JButton btnSpremeni_1 = new JButton("SPREMENI");
		btnSpremeni_1.setBounds(317, 160, 97, 23);
		panel_2.add(btnSpremeni_1);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setBounds(10, 36, 404, 2);
		panel_2.add(separator_3);
		
		JPanel panel_9 = new JPanel();
		tabbedPane_2.addTab("Dodeli zival", null, panel_9, null);
		SpringLayout sl_panel_9 = new SpringLayout();
		panel_9.setLayout(sl_panel_9);
		
		JLabel lblDodeli = new JLabel("DODELI ZIVAL:");
		sl_panel_9.putConstraint(SpringLayout.NORTH, lblDodeli, 10, SpringLayout.NORTH, panel_9);
		sl_panel_9.putConstraint(SpringLayout.WEST, lblDodeli, 10, SpringLayout.WEST, panel_9);
		panel_9.add(lblDodeli);
		
		JComboBox comboBox_17 = new JComboBox();
		sl_panel_9.putConstraint(SpringLayout.NORTH, comboBox_17, -3, SpringLayout.NORTH, lblDodeli);
		sl_panel_9.putConstraint(SpringLayout.WEST, comboBox_17, 11, SpringLayout.EAST, lblDodeli);
		sl_panel_9.putConstraint(SpringLayout.EAST, comboBox_17, 104, SpringLayout.EAST, lblDodeli);
		panel_9.add(comboBox_17);
		
		JLabel lblK = new JLabel("ZAPOSLENEMU:");
		sl_panel_9.putConstraint(SpringLayout.NORTH, lblK, 26, SpringLayout.SOUTH, lblDodeli);
		sl_panel_9.putConstraint(SpringLayout.WEST, lblK, 0, SpringLayout.WEST, lblDodeli);
		panel_9.add(lblK);
		
		JComboBox comboBox_18 = new JComboBox();
		sl_panel_9.putConstraint(SpringLayout.NORTH, comboBox_18, -3, SpringLayout.NORTH, lblK);
		sl_panel_9.putConstraint(SpringLayout.WEST, comboBox_18, 0, SpringLayout.WEST, comboBox_17);
		sl_panel_9.putConstraint(SpringLayout.EAST, comboBox_18, -238, SpringLayout.EAST, panel_9);
		panel_9.add(comboBox_18);
		
		JButton btnDodaj_1 = new JButton("DODAJ");
		sl_panel_9.putConstraint(SpringLayout.SOUTH, btnDodaj_1, -21, SpringLayout.SOUTH, panel_9);
		sl_panel_9.putConstraint(SpringLayout.EAST, btnDodaj_1, -10, SpringLayout.EAST, panel_9);
		panel_9.add(btnDodaj_1);
		
		JSeparator separator_6 = new JSeparator();
		sl_panel_9.putConstraint(SpringLayout.WEST, separator_6, 10, SpringLayout.WEST, panel_9);
		sl_panel_9.putConstraint(SpringLayout.SOUTH, separator_6, 0, SpringLayout.SOUTH, btnDodaj_1);
		panel_9.add(separator_6);
		
		JPanel panel_10 = new JPanel();
		tabbedPane_2.addTab("Odvzemi zival", null, panel_10, null);
		GridBagLayout gbl_panel_10 = new GridBagLayout();
		gbl_panel_10.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_10.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_panel_10.columnWeights = new double[]{0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_panel_10.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_10.setLayout(gbl_panel_10);
		
		JLabel lblNewLabel_9 = new JLabel("ODVZAMI");
		GridBagConstraints gbc_lblNewLabel_9 = new GridBagConstraints();
		gbc_lblNewLabel_9.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_9.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_9.gridx = 0;
		gbc_lblNewLabel_9.gridy = 0;
		panel_10.add(lblNewLabel_9, gbc_lblNewLabel_9);
		
		JLabel lblZival_1 = new JLabel("ZIVAL");
		GridBagConstraints gbc_lblZival_1 = new GridBagConstraints();
		gbc_lblZival_1.anchor = GridBagConstraints.EAST;
		gbc_lblZival_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblZival_1.gridx = 1;
		gbc_lblZival_1.gridy = 0;
		panel_10.add(lblZival_1, gbc_lblZival_1);
		
		JComboBox comboBox_15 = new JComboBox();
		GridBagConstraints gbc_comboBox_15 = new GridBagConstraints();
		gbc_comboBox_15.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_15.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_15.gridx = 2;
		gbc_comboBox_15.gridy = 0;
		panel_10.add(comboBox_15, gbc_comboBox_15);
		
		JLabel lblZival = new JLabel("");
		GridBagConstraints gbc_lblZival = new GridBagConstraints();
		gbc_lblZival.insets = new Insets(0, 0, 5, 5);
		gbc_lblZival.anchor = GridBagConstraints.EAST;
		gbc_lblZival.gridx = 5;
		gbc_lblZival.gridy = 0;
		panel_10.add(lblZival, gbc_lblZival);
		
		JLabel lblOskrbniku = new JLabel("ZAPOSLENEMU");
		GridBagConstraints gbc_lblOskrbniku = new GridBagConstraints();
		gbc_lblOskrbniku.insets = new Insets(0, 0, 5, 5);
		gbc_lblOskrbniku.gridx = 0;
		gbc_lblOskrbniku.gridy = 1;
		panel_10.add(lblOskrbniku, gbc_lblOskrbniku);
		
		JLabel label_1 = new JLabel("");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.anchor = GridBagConstraints.EAST;
		gbc_label_1.gridx = 1;
		gbc_label_1.gridy = 1;
		panel_10.add(label_1, gbc_label_1);
		
		JComboBox comboBox_16 = new JComboBox();
		GridBagConstraints gbc_comboBox_16 = new GridBagConstraints();
		gbc_comboBox_16.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_16.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_16.gridx = 2;
		gbc_comboBox_16.gridy = 1;
		panel_10.add(comboBox_16, gbc_comboBox_16);
		
		JButton btnOdvzemi = new JButton("ODVZEMI");
		GridBagConstraints gbc_btnOdvzemi = new GridBagConstraints();
		gbc_btnOdvzemi.insets = new Insets(0, 0, 0, 5);
		gbc_btnOdvzemi.gridx = 5;
		gbc_btnOdvzemi.gridy = 5;
		panel_10.add(btnOdvzemi, gbc_btnOdvzemi);
		
		JPanel Zival = new JPanel();
		tabbedPane.addTab("Zival", null, Zival, null);
		Zival.setLayout(null);
		
		JTabbedPane tabbedPane_3 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane_3.setBounds(0, 0, 429, 222);
		Zival.add(tabbedPane_3);
		
		JPanel panel_4 = new JPanel();
		tabbedPane_3.addTab("Ustvari", null, panel_4, null);
		panel_4.setLayout(null);
		
		JLabel lblTipivali = new JLabel("TIP \u017DIVALI:");
		lblTipivali.setForeground(SystemColor.textHighlight);
		lblTipivali.setBounds(10, 11, 73, 14);
		panel_4.add(lblTipivali);
		
		JComboBox comboBox_7 = new JComboBox();
		comboBox_7.setBounds(93, 8, 28, 20);
		panel_4.add(comboBox_7);
		
		JLabel lblIme_2 = new JLabel("IME:");
		lblIme_2.setBounds(20, 50, 46, 14);
		panel_4.add(lblIme_2);
		
		JLabel lblDatumRosjtva = new JLabel("DATUM ROSJTVA:");
		lblDatumRosjtva.setBounds(84, 50, 111, 14);
		panel_4.add(lblDatumRosjtva);
		
		JLabel lblTea = new JLabel("TE\u017DA:");
		lblTea.setBounds(219, 50, 59, 14);
		panel_4.add(lblTea);
		
		JLabel lblDolina = new JLabel("DOL\u017DINA:");
		lblDolina.setBounds(312, 50, 73, 14);
		panel_4.add(lblDolina);
		
		JSpinner spinner_2 = new JSpinner();
		spinner_2.setBounds(219, 75, 29, 20);
		panel_4.add(spinner_2);
		
		JSpinner spinner_3 = new JSpinner();
		spinner_3.setBounds(322, 75, 29, 20);
		panel_4.add(spinner_3);
		
		textField_10 = new JTextField();
		textField_10.setBounds(0, 75, 59, 20);
		panel_4.add(textField_10);
		textField_10.setColumns(10);
		
		textField_11 = new JTextField();
		textField_11.setBounds(82, 75, 86, 20);
		panel_4.add(textField_11);
		textField_11.setColumns(10);
		
		JLabel lblSpol = new JLabel("SPOL:");
		lblSpol.setBounds(278, 11, 46, 14);
		panel_4.add(lblSpol);
		
		JRadioButton rdbtnM = new JRadioButton("M");
		rdbtnM.setBounds(330, 7, 36, 23);
		panel_4.add(rdbtnM);
		
		JRadioButton radioButton = new JRadioButton("\u017D");
		radioButton.setBounds(368, 7, 46, 23);
		panel_4.add(radioButton);
		
		JButton btnUstvari_2 = new JButton("USTVARI");
		btnUstvari_2.setBounds(312, 160, 102, 23);
		panel_4.add(btnUstvari_2);
		
		JSeparator separator_4 = new JSeparator();
		separator_4.setBounds(0, 36, 424, 2);
		panel_4.add(separator_4);
		
		JPanel panel_5 = new JPanel();
		tabbedPane_3.addTab("Brisi/Spremeni", null, panel_5, null);
		panel_5.setLayout(null);
		
		JLabel lblNewLabel_3 = new JLabel("IZBRI�I �IVAL:");
		lblNewLabel_3.setForeground(SystemColor.textHighlight);
		lblNewLabel_3.setBounds(12, 16, 79, 14);
		panel_5.add(lblNewLabel_3);
		
		JComboBox comboBox_8 = new JComboBox();
		comboBox_8.setBounds(101, 13, 65, 20);
		panel_5.add(comboBox_8);
		
		JButton btnIzbrii_2 = new JButton("IZBRI\u0160I");
		btnIzbrii_2.setBounds(312, 12, 102, 23);
		panel_5.add(btnIzbrii_2);
		
		JSeparator separator_5 = new JSeparator();
		separator_5.setBounds(0, 51, 424, 2);
		panel_5.add(separator_5);
		
		JLabel lblSpremeniival = new JLabel("SPREMENI \u017DIVAL");
		lblSpremeniival.setForeground(SystemColor.textHighlight);
		lblSpremeniival.setBounds(12, 65, 86, 14);
		panel_5.add(lblSpremeniival);
		
		JComboBox comboBox_9 = new JComboBox();
		comboBox_9.setBounds(101, 62, 65, 20);
		panel_5.add(comboBox_9);
		
		JLabel lblNewLabel_4 = new JLabel("IME");
		lblNewLabel_4.setBounds(12, 106, 45, 14);
		panel_5.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("DOLZINA:");
		lblNewLabel_5.setBounds(79, 106, 65, 14);
		panel_5.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("DATUM ROJSTVA:");
		lblNewLabel_6.setBounds(168, 106, 115, 14);
		panel_5.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("TEZA:");
		lblNewLabel_7.setBounds(277, 106, 46, 14);
		panel_5.add(lblNewLabel_7);
		
		textField_12 = new JTextField();
		textField_12.setBounds(0, 136, 65, 20);
		panel_5.add(textField_12);
		textField_12.setColumns(10);
		
		JSpinner spinner_4 = new JSpinner();
		spinner_4.setBounds(89, 136, 29, 20);
		panel_5.add(spinner_4);
		
		JSpinner spinner_5 = new JSpinner();
		spinner_5.setBounds(277, 136, 29, 20);
		panel_5.add(spinner_5);
		
		textField_13 = new JTextField();
		textField_13.setBounds(165, 136, 86, 20);
		panel_5.add(textField_13);
		textField_13.setColumns(10);
		
		JLabel lblSpol_1 = new JLabel("SPOL:");
		lblSpol_1.setBounds(176, 64, 46, 14);
		panel_5.add(lblSpol_1);
		
		JRadioButton rdbtnM_1 = new JRadioButton("M");
		rdbtnM_1.setBounds(214, 51, 37, 14);
		panel_5.add(rdbtnM_1);
		
		JRadioButton radioButton_1 = new JRadioButton("\u017D");
		radioButton_1.setBounds(214, 65, 37, 14);
		panel_5.add(radioButton_1);
		
		JLabel lblTip_2 = new JLabel("TIP:");
		lblTip_2.setBounds(277, 64, 46, 14);
		panel_5.add(lblTip_2);
		
		JComboBox comboBox_11 = new JComboBox();
		comboBox_11.setBounds(342, 62, 28, 20);
		panel_5.add(comboBox_11);
		
		JButton btnSpremeni_2 = new JButton("SPREMENI");
		btnSpremeni_2.setBounds(312, 160, 102, 23);
		panel_5.add(btnSpremeni_2);
		
		JPanel panel_6 = new JPanel();
		tabbedPane.addTab("Zivila", null, panel_6, null);
		panel_6.setLayout(null);
		
		JLabel lblDodajZivilo = new JLabel("DODAJ ZIVILO:");
		lblDodajZivilo.setForeground(SystemColor.textHighlight);
		lblDodajZivilo.setBounds(10, 11, 81, 14);
		panel_6.add(lblDodajZivilo);
		
		JLabel lblNaziv_2 = new JLabel("NAZIV:");
		lblNaziv_2.setBounds(45, 48, 46, 14);
		panel_6.add(lblNaziv_2);
		
		JLabel lblTeza = new JLabel("TEZA:");
		lblTeza.setBounds(149, 48, 46, 14);
		panel_6.add(lblTeza);
		
		JLabel lblRokUporabe = new JLabel("ROK UPORABE:");
		lblRokUporabe.setBounds(235, 48, 86, 14);
		panel_6.add(lblRokUporabe);
		
		textField_14 = new JTextField();
		textField_14.setBounds(23, 73, 86, 20);
		panel_6.add(textField_14);
		textField_14.setColumns(10);
		
		JSpinner spinner_6 = new JSpinner();
		spinner_6.setBounds(144, 73, 29, 20);
		panel_6.add(spinner_6);
		
		textField_15 = new JTextField();
		textField_15.setBounds(235, 73, 86, 20);
		panel_6.add(textField_15);
		textField_15.setColumns(10);
		
		JButton btnDodajZivilo = new JButton("DODAJ ZIVILO");
		btnDodajZivilo.setBounds(288, 188, 131, 23);
		panel_6.add(btnDodajZivilo);
		
		JPanel panel_11 = new JPanel();
		tabbedPane.addTab("Odjava/prijava", null, panel_11, null);
		panel_11.setLayout(null);
		
		JLayeredPane layeredPane = new JLayeredPane();
		layeredPane.setBounds(10, 47, 409, 164);
		panel_11.add(layeredPane);
		layeredPane.setLayout(new CardLayout(0, 0));
		
		panel_12 = new JPanel();
		layeredPane.add(panel_12, "name_434363839148000");
		
		JLabel lblOdjavaUspena = new JLabel("ODJAVA USPE\u0160NA");
		lblOdjavaUspena.setForeground(new Color(0, 255, 0));
		panel_12.add(lblOdjavaUspena);
		
		panel_13 = new JPanel();
		layeredPane.add(panel_13, "name_434403515491100");
		
		JLabel lblAljazaeubergsn = new JLabel("ALJAZ_NEUBERG_SN5");
		panel_13.add(lblAljazaeubergsn);
		
		JPanel panel_14 = new JPanel();
		layeredPane.add(panel_14, "name_435334189345700");
		
		JLabel lblUporabniskoIme = new JLabel("Uporabnisko ime:");
		panel_14.add(lblUporabniskoIme);
		
		textField_16 = new JTextField();
		panel_14.add(textField_16);
		textField_16.setColumns(10);
		
		JLabel lblGeslo = new JLabel("Geslo:");
		panel_14.add(lblGeslo);
		
		textField_17 = new JTextField();
		panel_14.add(textField_17);
		textField_17.setColumns(10);
		
		JButton btnPrijava = new JButton("Prijava");
		panel_14.add(btnPrijava);
		
		JButton odjava = new JButton("Odjava");
		odjava.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				layeredPane.removeAll();
				layeredPane.add(panel_12);
				layeredPane.repaint();
				layeredPane.revalidate();
			}
		});
		odjava.setBounds(10, 11, 89, 23);
		panel_11.add(odjava);
		
		JButton info = new JButton("Info:");
		info.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				layeredPane.removeAll();
				layeredPane.add(panel_13);
				layeredPane.repaint();
				layeredPane.revalidate();
				
			}
		});
		info.setBounds(164, 11, 89, 23);
		panel_11.add(info);
		
		JButton btnNewButton = new JButton("Prijava");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				layeredPane.removeAll();
				layeredPane.add(panel_14);
				layeredPane.repaint();
				layeredPane.revalidate();
			}
		});
		btnNewButton.setBounds(307, 11, 89, 23);
		panel_11.add(btnNewButton);
	}
}
