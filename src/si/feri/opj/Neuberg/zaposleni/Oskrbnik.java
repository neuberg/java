package si.feri.opj.Neuberg.zaposleni;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;

import si.feri.opj.Neuberg.Kmetija.Zival;
import si.feri.opj.Neuberg.Kmetija.Zivilo;
import si.feri.opj.Neuberg.Objekti.Akvarij;
import si.feri.opj.Neuberg.Objekti.Kletka;
import si.feri.opj.Neuberg.Objekti.Ograda;
import si.feri.opj.Neuberg.Ostalo.IVstop;

/**
 * @author Aljaz Neuberg
 * @version 1.0
 *
 */

public class Oskrbnik implements IVstop, Serializable {

	private String ime, priimek;
	private int telefon;
	private Zival[] zivali = new Zival[10];

	private Oskrbnik() {

	}
  
	private Oskrbnik(String ime, String priimek) {

		this.ime = ime;
		this.priimek = priimek;
	}

	public Oskrbnik(String ime, String priimek, int telefon) {

		this(ime, priimek);
		this.telefon = telefon;

	}

	//
	@Override
	public boolean lahkoVztopi(Ograda ograda)  {
		boolean vrni=false;
		
	
		for(int i=0;i<vrniSteviloZivali();i++) {
				for(int j=0;j<ograda.getKapaciteta();j++) {  
				if(zivali[i].equals(ograda.getZivali()[j])) {
					 vrni=true;
					 break;
				}
		}               
		}
		
		
	
	return vrni;
	
	
	
	
		
		
		
		
	}

	@Override
	public String toString() {
		return "[ime=" + ime + ", priimek=" + priimek + ", telefon=" + telefon + ", zivali=" + Arrays.toString(zivali)
				+ "]";
	}

	/**
	 * 
	 * @param x //vnesemo datum uporabe zivila
	 * @return // (vrne true ali false)
	 */
	public boolean hranaUzitna(Zivilo x) {

		if (x.getRokUporabe().isBefore(LocalDate.now())) {
			return false;
		} else if (x.getRokUporabe().isEqual(LocalDate.now())) {
			return true;
		} else
			return true;

	}

	/**
	 * 
	 * @param zival //zival, ki jo dodamo v array
	 */
	public void dodeliZival(Zival zival) {
		for (int i = 0; i < zivali.length; i++) {
			if (zivali[i] == null) {
				zivali[i] = zival;
				break;
			}

		}
	}

	/**
	 * 
	 * @param zival //zival, ki jo odstranimo iz array-ja
	 */
	public void odvzemiZival(Zival zival) {
		for (int i = 0; i < zivali.length; i++) {
			if (zivali[i].equals(zival)) {
				zivali[i] = null;
				break;
			}

		}
	}

	/**
	 * 
	 * @return //vrne koliko zivali je v array-ju
	 */
	public int vrniSteviloZivali() {

		int stevec = 0;
		for (int i = 0; i < zivali.length; i++) {
			if (zivali[i] != null) {
				stevec++;
			}

		}

		return stevec;
	}

	/**
	 * 
	 * @param ime //z vnosom ime preverimo ce obstaja zival z enakim imenom
	 * @return //(vrne true ali false)
	 */
	public boolean zivalObstaja(String ime) {

		boolean obstaja = false;
		for (int i = 0; i < zivali.length; i++) {
			if (zivali[i] != null) {
				if (zivali[i].getIme().equals(ime)) {
					obstaja = true;
				}
			}
		}
		return obstaja;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPriimek() {
		return priimek;
	}

	public void setPriimek(String priimek) {
		this.priimek = priimek;
	}

	public int getTelefon() {
		return telefon;
	}

	public void setTelefon(int telefon) {
		this.telefon = telefon;
	}

}
