package si.feri.opj.Neuberg.zaposleni;

import java.io.Serializable;
import java.util.Arrays;

public class Prostovoljec extends Oskrbnik implements Serializable {

	public Prostovoljec(String ime, String priimek, int telefon) {
		super(ime, priimek, telefon);

	}

	@Override
	public String toString() {
		return "Prostovoljec: [" + super.toString() + "]";
	}

}
