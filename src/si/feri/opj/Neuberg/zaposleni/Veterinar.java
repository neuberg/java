package si.feri.opj.Neuberg.zaposleni;

import java.io.Serializable;

public class Veterinar extends Oskrbnik implements Serializable{

	private int stevilkaDovoljena;

	public Veterinar(String ime, String priimek, int telefon, int stevilkaDovoljena) {
		super(ime, priimek, telefon);
		this.stevilkaDovoljena = stevilkaDovoljena;
	}

	@Override
	public String toString() {
		return "Veterinar [stevilkaDovoljena=" + stevilkaDovoljena + ", " + super.toString() + "]";
	}

	public int getStevilkaDovoljena() {
		return stevilkaDovoljena;
	}

	public void setStevilkaDovoljena(int stevilkaDovoljena) {
		this.stevilkaDovoljena = stevilkaDovoljena;
	}

}
