package si.feri.opj.Neuberg.Ostalo;

import si.feri.opj.Neuberg.Objekti.Ograda;

public interface IVstop {

	public boolean lahkoVztopi(Ograda ograda);
    
	
}
