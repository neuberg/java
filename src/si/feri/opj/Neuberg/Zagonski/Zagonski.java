package si.feri.opj.Neuberg.Zagonski;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;


import si.feri.opj.Neuberg.Kmetija.TipZivali;
import si.feri.opj.Neuberg.Kmetija.Zival;
import si.feri.opj.Neuberg.Kmetija.Zivilo;
import si.feri.opj.Neuberg.Objekti.Akvarij;
import si.feri.opj.Neuberg.Objekti.Kletka;
import si.feri.opj.Neuberg.Objekti.Ograda;
import si.feri.opj.Neuberg.Ostalo.OgradaPolnaException;
import si.feri.opj.Neuberg.Ostalo.PremladaZivalException;
import si.feri.opj.Neuberg.Ostalo.TipZivaliException;
import si.feri.opj.Neuberg.zaposleni.Oskrbnik;
import si.feri.opj.Neuberg.zaposleni.Prostovoljec;
import si.feri.opj.Neuberg.zaposleni.Veterinar;

public class Zagonski {

	public static void main(String[] args) throws TipZivaliException {

		LocalDate rok1 = LocalDate.of(2015, 1, 1);
		LocalDate rok2 = LocalDate.of(2019, 2, 6);
		LocalDate rok3 = LocalDate.of(2016, 2, 1);
		LocalDate rok4 = LocalDate.of(2020, 1, 1);

		Zival zival = new Zival("Janez", rok1, 1, 1, "moski", TipZivali.RIBA);
		Zival zival2 = new Zival("�tef", rok2, 10, 17, "moski", TipZivali.RIBA);
		Zival zival3 = new Zival("Ivana", rok3, 15, 2, "zenski", TipZivali.PTIC);
		Zival zival1 = new Zival("Stankec", rok1, 34, 55, "moski", TipZivali.RIBA);
		Zival z = new Zival("Bojan", rok2, 22, 23, "zenski", TipZivali.SESALEC);
		Zival zival4 = new Zival("Berti", rok4, 22, 23, "zenski", TipZivali.SESALEC);
		
		Zival zival11 = new Zival("Janez", rok1, 1, 1, "moski", TipZivali.RIBA);
		Zival zival22 = new Zival("�tef", rok2, 10, 17, "moski", TipZivali.RIBA);
	

		System.out.println("/////////////////////////////////SN3,4/////////////////////////////////////////////////");

		ArrayList<Veterinar> veterinar = new ArrayList<Veterinar>();
		
		Veterinar v1 = new Veterinar("Ivan", "Ivanovic", 122,002);
		Veterinar v2 = new Veterinar("Adam", "Hegedis", 10,003);
		
		veterinar.add(v1);
		veterinar.add(v2);
		
		try {
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("Veterinar.ser"));
			objectOutputStream.writeObject(veterinar);
			objectOutputStream.flush();
			objectOutputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("Veterinar.ser"));
			ArrayList<Veterinar> temp = (ArrayList<Veterinar>) inputStream.readObject();
			inputStream.close();
			
			for(Veterinar k : temp)
				System.out.println(k);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	ArrayList<Oskrbnik> oskrbnik = new ArrayList<Oskrbnik>();
		
		Oskrbnik o1 = new Oskrbnik("Janez", "Novak", 122);
		Oskrbnik o2 = new Oskrbnik("�tef", "Novak2", 10);
		
		oskrbnik.add(o1);
		oskrbnik.add(o2);
		
		try {
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("Oskrbnik.ser"));
			objectOutputStream.writeObject(oskrbnik);
			objectOutputStream.flush();
			objectOutputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("Oskrbnik.ser"));
			ArrayList<Oskrbnik> temp = (ArrayList<Oskrbnik>) inputStream.readObject();
			inputStream.close();
			
			for(Oskrbnik k : temp)
				System.out.println(k);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	ArrayList<Prostovoljec> prostovoljec = new ArrayList<Prostovoljec>();
		
	Prostovoljec p1 = new Prostovoljec("Peter", "Vogrin",29872);
	Prostovoljec p2 = new Prostovoljec("Sara", "Kocbek",34009);
		
	prostovoljec.add(p1);
	prostovoljec.add(p2);
		
	try {
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("Prostovoljec.ser"));
		objectOutputStream.writeObject(prostovoljec);
		objectOutputStream.flush();
		objectOutputStream.close();
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
	
	try {
		ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("Prostovoljec.ser"));
		ArrayList<Prostovoljec> temp = (ArrayList<Prostovoljec>) inputStream.readObject();
		inputStream.close();
		
		for(Prostovoljec k : temp)
			System.out.println(k);
		
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		e.printStackTrace();
	}
	
		
		ArrayList<Zival> zivali = new ArrayList<Zival>();
		zivali.add(zival11);
		zivali.add(zival22);
		
		try {
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("zivali.ser"));
			objectOutputStream.writeObject(zivali);
			objectOutputStream.flush();
			objectOutputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("zivali.ser"));
			ArrayList<Zival> temp = (ArrayList<Zival>) inputStream.readObject();
			inputStream.close();
			
			for(Zival k : temp)
				System.out.println(k);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		ArrayList<Akvarij> akvarij = new ArrayList<Akvarij>();
	  Akvarij a1 = new Akvarij("aqua1", 2, TipZivali.RIBA);
		akvarij.add(a1);
		
		try {
			a1.dodajZival(zival);
			a1.dodajZival(zival2);
			a1.dodajZival(zival1);
		} catch (PremladaZivalException e) {
			
			e.printStackTrace();
		} catch (OgradaPolnaException e) {

			e.printStackTrace();
		}
		catch (TipZivaliException e) {

			e.printStackTrace();
		}
		
		try {
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("Akvarij.ser"));
			objectOutputStream.writeObject(akvarij);
			objectOutputStream.flush();
			objectOutputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("akvarij.ser"));
			ArrayList<Akvarij> temp = (ArrayList<Akvarij>) inputStream.readObject();
			inputStream.close();
			
			for(Akvarij k : temp)
				System.out.println(k);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		
		Oskrbnik o=new Oskrbnik("Bojan","Novak",193498);
		
		o.dodeliZival(zival);
		o.dodeliZival(zival2);
		
		
		System.out.println("AKVARIJ: ");
		Akvarij a = new Akvarij("aqua", 2, TipZivali.RIBA);
		try {
			a.dodajZival(zival);
			a.dodajZival(zival2);
		} catch (PremladaZivalException e) {
			
			e.printStackTrace();
		} catch (OgradaPolnaException e) {

			e.printStackTrace();
		}
		catch (TipZivaliException e) {

			e.printStackTrace();
		}
		
		System.out.println("Oskrbnik lahko vztopi: "+o.lahkoVztopi(a));
		System.out.println("Zival lahko vztopi: "+zival2.lahkoVztopi(a));
		System.out.println("Zival lahko vztopi: "+z.lahkoVztopi(a));
	
		
		System.out.println(a + " Zasedenost: " + a.vrniZasedenost());
		System.out.println("Starost zivali(P(Leta,Mesec,Dan)): " + zival.izracunajStarostZivali());
		System.out.println("Starost zivali(P(Leta,Mesec,Dan)): " + zival.izracunajStarostZivali());
		System.out.println(a + " Zasedenost: " + a.vrniZasedenost());
		System.out.println("Odstranimo vse zivali///");
		a.odstraniZivali();
		System.out.println(a + " Zasedenost: " + a.vrniZasedenost());

		System.out.println();

		
		
		
		System.out.println("KLETKA(true): ");
		ArrayList<Kletka> kletka = new ArrayList<Kletka>();
	  Kletka k = new Kletka("aqua", true, 4, TipZivali.PTIC);
		kletka.add(k);
		
		System.out.println(k + " Zasedenost: " + k.vrniZasedenost());
		try {
			k.dodajZival(zival);
		} catch (PremladaZivalException e) {

			e.printStackTrace();
		} catch (OgradaPolnaException e) {

			e.printStackTrace();
		} catch (TipZivaliException e) {

			e.printStackTrace();
		}
		System.out.println("Starost zivali(P(Leta,Mesec,Dan)): " + zival.izracunajStarostZivali());
		System.out.println(k + " Zasedenost: " + k.vrniZasedenost());
		try {
			k.dodajZival(zival4);
		} catch (PremladaZivalException e) {

			e.printStackTrace();
		} catch (OgradaPolnaException e) {

			e.printStackTrace();
		}
	catch (TipZivaliException e) {

		e.printStackTrace();
	}
	
	try {
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("Kletka.ser"));
			objectOutputStream.writeObject(kletka);
			objectOutputStream.flush();
			objectOutputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("Kletka.ser"));
			ArrayList<Kletka> temp = (ArrayList<Kletka>) inputStream.readObject();
			inputStream.close();
			
			for(Kletka kl : temp)
				System.out.println(k);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		System.out.println("Starost zivali(P(Leta,Mesec,Dan)): " + zival.izracunajStarostZivali());
		System.out.println(k + " Zasedenost: " + k.vrniZasedenost());
		System.out.println("Odstranimo vse zivali///");
		k.odstraniZivali();
		System.out.println(a + " Zasedenost: " + a.vrniZasedenost());

		/*System.out.println("//////////////////////////////////////////////////");
		System.out.println("Kletka false");
		Kletka c = new Kletka("aqua", false, -4, TipZivali.PTIC);
		System.out.println(c + " Zasedenost: " + c.vrniZasedenost());
		try {
			c.dodajZival(zival3);
		} catch (PremladaZivalException e) {

			e.printStackTrace();
		} catch (OgradaPolnaException e) {

			e.printStackTrace();
		}
		catch (TipZivaliException e) {

			e.printStackTrace();
		}
		System.out.println("Starost zivali(P(Leta,Mesec,Dan)): " + zival.izracunajStarostZivali());
		System.out.println(c + " Zasedenost: " + c.vrniZasedenost());
*/
	}

}
